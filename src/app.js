    
   const config = {
    numParticles: 30,
    particleRadius: 4,
    fillStyle: "#000000",
    strokeStyle: {r: 255, g: 153, b: 187} ,
    lineWidth: 0.3 ,
    maxParicleDistance: 200  
}
  
  
  class Particle {
            constructor(width,height, radius) {
                this.width = width;
                this.height = height;            
                this.velocityMax = 1;
                this.x = ~~(Math.random() * width);
                this.y = ~~(Math.random() * height);
                this.xVelocity = this.velocityMax * Math.random() * (~~(Math.random() * 2) ? -1 : 1);
                this.yVelocity = this.velocityMax * Math.random() * (~~(Math.random() * 2) ? -1 : 1);
                this.radius = ((Math.random() + 0.5) * radius);
            }
            move(changeDirection) {              
                if (this.x + this.xVelocity <= 0 || this.x + this.xVelocity >= this.width || changeDirection) {
                    this.xVelocity *= -1;
                }
                if (this.y + this.yVelocity <= 0 || this.y + this.yVelocity >=  this.height||changeDirection) {
                    this.yVelocity *= -1;
                }
                this.x += this.xVelocity;
                this.y += this.yVelocity;
            }  
                  
        };

              
    window.onload = function() {                  
      const canvas = document.createElement('canvas');  
      const context = canvas.getContext('2d');
      const numParticles = config.numParticles;
      const particleRadius = config.particleRadius;
      const maxParicleDistance = config.maxParicleDistance;
      const strokeStyle = config.strokeStyle
      const radiusFactor = 1.3

      const calculateColor = function(r,g,b,a) {
            return `rgba(${r}, ${g}, ${b}, ${a})`;
        };
               
      const drawParticles = function() {        
        for (let i = 0; i < particles.length; i++ ) {  
            let particle = particles[i]  ;   
            
            context.beginPath();
            context.arc(particle.x, particle.y, particle.radius + radiusFactor, 0, 2* Math.PI, 0);
            context.fillStyle = basicStrokeStyle;
            context.fill();
            context.stroke();
            context.closePath();

            context.beginPath();
            context.arc(particle.x, particle.y, particle.radius, 0, 2* Math.PI, 0);
            context.fillStyle = config.fillStyle;
            context.fill();
            context.closePath();

            particle.move(false);
        }
      };

      const connectParticles = function() {      
        for (let i = 0; i < particles.length; i++ ) {                
            for (let j = 0; j < particles.length; j++ ) {
              if (particles[i] !== particles[j]) {
                  let distance = findDistance(particles[i], particles[j]);
                  let minDistance = (particles[i].radius + particles[j].radius )+ 2* radiusFactor
                if (distance < maxParicleDistance && distance > minDistance) {
                  context.beginPath();
                  context.lineWidth =  (maxParicleDistance/distance) * config.lineWidth
                  context.strokeStyle = calculateColor(strokeStyle.r, strokeStyle.g, strokeStyle.b, (maxParicleDistance/distance) - 1)
                 
                  context.moveTo(particles[i].x, particles[i].y);
                  context.lineTo(particles[j].x, particles[j].y);
                  context.stroke();
                  
                  context.closePath();              
                }  
                else if (distance <= minDistance) {
                    particles[i].move(true);
                    particles[j].move(true);
                }     
          }
        }      
      }
    }
  
      const findDistance = function (point1,point2){  
         return Math.sqrt( Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2) );
       }

      const createParticles = function() {         
        if(particles.length < numParticles)     
        for (let i  = 0; i < numParticles; i++) {
            particles.push(new Particle(canvas.width,canvas.height, particleRadius));       
        }     
     }
         
    const animationLoop = function() {
        clear();
        update();
        draw();
        queue();   
    }

    const clear = function() {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    const update = function(){
        createParticles();
        connectParticles();
    }

    const draw = function(){
        drawParticles();
    }

    const queue = function() {
        setTimeout(function() {
            window.requestAnimationFrame(animationLoop);
        }, 1000/40);
      
    }

      const basicStrokeStyle = calculateColor(strokeStyle.r, strokeStyle.g, strokeStyle.b, 1);
      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;          
      let particles = [];   
      document.body.appendChild(canvas);    

      animationLoop()
    
    };
